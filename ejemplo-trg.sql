drop database if exists ejemplo_trg;
create database ejemplo_trg;

\c ejemplo_trg

create table usuarie (id int, nombre text);
create table alerta  (id int, name text);

insert into usuarie values (1, 'Eva');
insert into usuarie values (2, 'Patricio');
insert into usuarie values (3, 'Marisa');

create or replace function cambio_de_nombre() returns trigger as $$
begin
	if new.nombre != old.nombre then
		insert into alerta values (old.id, old.nombre);
	end if;
	return new;
end;
$$ language plpgsql;

create trigger cambio_de_nombre_trg
before update on usuarie
for each row
execute procedure cambio_de_nombre();
