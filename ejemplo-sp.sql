drop database if exists ejemplo_sp;
create database ejemplo_sp;

\c ejemplo_sp

create or replace function suma(a int, b int) returns int as $$
declare
	c int;
begin
	c := a + b;
	return c;
end;
$$ language plpgsql;
